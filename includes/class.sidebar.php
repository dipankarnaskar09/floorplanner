<?php

if (isset($_SESSION['user_type'])) {
	echo $_SESSION['user_type'];
}
else{
	echo 'user type not set';
}
exit;
// if ($_SESSION['user_type'] == 'Client') {
// 	$sql1 = "SELECT * FROM menu WHERE client_access = 'yes' AND menu_id = '".$pmenu."'";
// 	$sql2 = "SELECT * FROM menu WHERE client_access = 'yes' AND menu_id = '".$pmenu."'";
// 	$sql3 = "SELECT * FROM menu WHERE client_access = 'yes' AND menu_id = '".$pmenu."'";
// 	$sql4 = "SELECT * FROM menu WHERE client_access = 'yes' AND menu_id = '".$pmenu."'";
// } else {

// }


final class sidebar
{
	private $baseurl="";
	private $menu="";
	private $submenu = "";

	private $conn;

	private $menulist = array();

	function __construct($menu,$submenu)
	{
		$this->menu = $menu;
		$this->submenu = $submenu;
	}


	// ========== Start private methods==============//
	private function connect($host,$user,$pwd,$db){
		$this->conn = @mysqli_connect($host,$user,$pwd,$dbName) or die("Could not connect to database for menu creation.");
	}

	// Function for generating menu list //
	private function getmenulist(){
		$tmp = array(); // holder of all the menu informations //

		$menus   = $this->getMenuByDept();   // Getting the department assigned menu id //
		$parents = $this->getParentMenus($menus); // Getting the parent_id of the assigned menus //

		//print_r($menus); exit;

		foreach($parents as $pmenu){
			//$qry = "SELECT * FROM menu WHERE menu_id = '".$pmenu."'";
			$qrs = @mysqli_query($con, "SELECT * FROM menu WHERE menu_id = '".$pmenu."'");
			if(@mysqli_num_rows($qrs)>0){
				while($data = @mysqli_fetch_assoc($qrs)){
					$tmp_sub = array();
					$rs_sub = @mysqli_query($con, "SELECT * FROM menu WHERE parent_id = '".$data['menu_id']."' AND menu_id IN(".join(",",$menus).")");
					if(@mysqli_num_rows($rs_sub)>0){
						while($data_sub = @mysqli_fetch_assoc($rs_sub)){
							$tmp_sub[] = $data_sub;
						}
					}

					// Building the main menu array //
					$tmp[] = array(
							  'name' => $data['menu_name'],
							  'icon'=> $data['icon'],
							  'submenu' => $tmp_sub
							 );

				}
			}
		}

		$this->menulist = $tmp;

	}

	private function getMenuByDept(){
		$parents = array();
		$dq = @mysqli_query($con, "SELECT menu_id FROM menu WHERE parent_id != '0' ORDER BY menu_id ASC");

		if(@mysqli_num_rows($dq)>0){
			while($tmp = @mysqli_fetch_assoc($dq)){
				$parents[] = $tmp['menu_id'];
			}
		}
		//print_r($parents); exit();
		return $parents;
	}

	private function getParentMenus($menu_arr){
		$arr = array();
		$mrs = @mysqli_query($con, "SELECT menu_id FROM menu WHERE parent_id = '0' ORDER BY menu_id ASC");
		if(@mysqli_num_rows($mrs)>0){
			while($dat = @mysqli_fetch_assoc($mrs)){
				$arr[] = $dat['menu_id'];
			}
		}
		//print_r($arr);exit();
		return $arr;
	}

	// ========== End private methods==============//

	// ========== Start public methods==============//
	public function initConnection($dbHost,$dbUsername,$dbPassword,$dbName)
	{
		$this->connect($dbHost,$dbUsername,$dbPassword,$dbName);
	}

	public function set_baseUrl($url)
	{
		$this->baseurl = $url;
	}

	public function createMenu()
	{
		// Generating menu array //
		$this->getmenulist();
		echo '<aside class="main-sidebar"><section class="sidebar">';
		echo '<ul class="sidebar-menu">';
		$i = 0;
		foreach($this->menulist as $menu )
		{
			$li_active = '';
			$li_class = '';
			if($this->menu == $i)
			{
				$li_class = 'active treeview';
				$ulclass = ' class = "active treeview"';
				$flag=true;
				$li_active =  '<a class="dropdown-toggle" href="#">
                    		  <i class="'.$menu['icon'].'"></li>';
			}else{
				$ulclass = ' class = "treeview"';
				$li_active =  '<a href="#">
                    		  <i class="'.$menu['icon'].'"></li>';
			}

			$flag=false;
			echo '<li class="'.$li_class.'">'.$li_active.'<span>'.$menu['name'].'</span><i class="fa fa-angle-left pull-right"></li></a>';
			$subarr = $menu['submenu']; // Select The submenu list array key //
			$pin = 0;

			// Start Inner UL //
			echo '<ul'.$ulclass.'>';
			foreach($subarr as $submenu_info)
			{
				// Start inner LI tag //
				// $liclass = ($flag == true and $this->submenu == $pin++) ? 'current' : '';
				// $liclass = (basename($_SERVER['PHP_SELF']) ==  $submenu_info['menu_page']) ? 'current' : '';
				$liclass = '';
				if( (basename($_SERVER['PHP_SELF']) ==  $submenu_info['menu_page']) || $this->submenu == $submenu_info['menu_id']){
					$liclass = 'active';
				}


				if(!empty($submenu_info['menu_page'])){
					echo '<li><a href="'.$this->baseurl.$submenu_info['menu_page'].'"><i class="'.$liclass.'"></i>'.$submenu_info['menu_name'].'</a></li>';
				}else{
					echo '<li><a href="javascript:void(0)">'.$submenu_info['menu_name'].'</a></li>';
				}

			}
			echo '</ul>';
			$i++;
		}

		echo '</ul>';
		echo '</section></aside>';
	}

	// ========== End public methods==============//

} //  end class //
?>