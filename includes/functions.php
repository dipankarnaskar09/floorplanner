<?php
include('config.php');
// echo $_SESSION['user_type'];

function createMenu($menu, $sub_menu){

	// Generating menu array //
	// $baseUrl           = 'http://coredigita.com/floorplanner/';
	$con               = mysqli_connect('localhost', 'dipankar', 'dipankar@123', 'db_floorplanner');
	$menulist          = getmenulist($menu, $sub_menu);
	echo '<aside class ="main-sidebar"><section class="sidebar">';
	echo '<ul class    ="sidebar-menu">';
	$i                 = 0;
	foreach($menulist as $menu )
	{
		$li_active = '';
		$li_class = 'treeview';
		if($menu == $i) {
			$li_class = 'active treeview';
			$ulclass  = ' class = "active treeview"';
			$flag     = true;
			$li_active =  '<a class="dropdown-toggle" href="#">
                		  <i class="'.$menu['icon'].'"></i>';
		}else {
				$ulclass   = ' class = "treeview-menu"';
				$li_active =  '<a href="#">
				<i class   ="'.$menu['icon'].'"></i>';
		}

		$flag = false;
		echo '<li class="'.$li_class.'">'.$li_active.'<span>'.$menu['name'].'</span><i class="fa fa-angle-left pull-right"></i></a>';
		$subarr = $menu['submenu']; // Select The submenu list array key //
		$pin = 0;

		// Start Inner UL //
		echo '<ul'.$ulclass.'>';
		foreach($subarr as $submenu_info)
		{
			// Start inner LI tag //
			// $liclass = ($flag == true and $this->submenu == $pin++) ? 'current' : '';
			// $liclass = (basename($_SERVER['PHP_SELF']) ==  $submenu_info['menu_page']) ? 'current' : '';
			$liclass = '';
			if( (basename($_SERVER['PHP_SELF']) ==  $submenu_info['menu_page']) || $submenu == $submenu_info['menu_id']){
				$liclass = 'active';
			}


			if(!empty($submenu_info['menu_page'])){
				echo '<li class="'.$liclass.'"><a href="'.$baseUrl.$submenu_info['menu_page'].'">'.$submenu_info['menu_name'].'</a></li>';
			}else{
				echo '<li><a href="javascript:void(0)">'.$submenu_info['menu_name'].'</a></li>';
			}

		}
		echo '</ul>';
		$i++;
	}

	echo '</ul>';
	echo '</section></aside>';
}

function getmenulist($menu, $sub_menu){
	// global $user_type;
	//echo $user_type;
	// $con      = mysqli_connect('localhost', 'dipankar', 'dipankar@123', 'db_floorplanner');
	$tmp      = array(); // holder of all the menu informations //

	$menus   = getMenuByDept();   // Getting the department assigned menu id //

	$parents = getParentMenus($menus); // Getting the parent_id of the assigned menus //

	// print_r($menus); exit;
	// print_r($parents); exit;

	foreach($parents as $pmenu){
		$con = mysqli_connect('localhost', 'dipankar', 'dipankar@123', 'db_floorplanner');
		$qrs = @mysqli_query($con, "SELECT * FROM menu WHERE menu_id = '".$pmenu."'");
		if(@mysqli_num_rows($qrs)>0){
			while($data = @mysqli_fetch_assoc($qrs)){
				$tmp_sub = array();
				$rs_sub = @mysqli_query($con, "SELECT * FROM menu WHERE parent_id = '".$data['menu_id']."' AND menu_id IN(".join(",",$menus).")");
				// print_r($rs_sub); exit;
				if(@mysqli_num_rows($rs_sub)>0){
					while($data_sub = @mysqli_fetch_assoc($rs_sub)){
						$tmp_sub[] = $data_sub;
					}
				}

				// Building the main menu array //
				$tmp[] = array(
						  'name' => $data['menu_name'],
						  'icon'=> $data['icon'],
						  'submenu' => $tmp_sub
						 );

			}
		}
	}

	return $tmp;

}

function getMenuByDept(){
	$parents = array();
	$con     = mysqli_connect('localhost', 'dipankar', 'dipankar@123', 'db_floorplanner');

	if($_SESSION['user_type'] == "Admin"){
		$dq = mysqli_query($con, "SELECT menu_id FROM menu WHERE parent_id != '0' AND admin_access = 'yes' ORDER BY menu_id ASC");
	}
	else if($_SESSION['user_type'] == "Client"){
		$dq = mysqli_query($con, "SELECT menu_id FROM menu WHERE parent_id != '0' AND client_access = 'yes' ORDER BY menu_id ASC");
	}

	if(@mysqli_num_rows($dq)>0){
		while($tmp = @mysqli_fetch_assoc($dq)){
			$parents[] = $tmp['menu_id'];
		}
	}
	//print_r($parents); exit();
	return $parents;
}

function getParentMenus($menu_arr){
	$arr = array();
	$con = mysqli_connect('localhost', 'dipankar', 'dipankar@123', 'db_floorplanner');

	// 
	if($_SESSION['user_type'] == "Admin"){
		$mrs = @mysqli_query($con, "SELECT DISTINCT(parent_id) FROM menu WHERE menu_id IN (".join(",",$menu_arr).") ORDER BY parent_id ASC");
		if(@mysqli_num_rows($mrs)>0){
			while($dat = @mysqli_fetch_assoc($mrs)){
				$arr[] = $dat['parent_id'];
			}
		}
	}else{			
		if(!empty($menu_arr)){
			$mrs = @mysqli_query($con, "SELECT DISTINCT(parent_id) FROM menu WHERE menu_id IN (".join(",",$menu_arr).") ORDER BY parent_id ASC");
			if(@mysqli_num_rows($mrs)>0){

				while($dat = @mysqli_fetch_assoc($mrs)){
					$arr[] = $dat['parent_id'];
				}
			}
		}
	}
	return $arr;
}

?>