<?php
	header('Content-Type: application/json');
	include('../includes/config.php');
	include('../includes/all_functions.php');
	include('../class/imageGrid.class.php');
	require_once('../class/class.phpmailer.new.php');
	require_once('../class/Class.EmailerApi.php');
	$prj_id = encryptorDecryptor('decrypt',$_POST["prj_id"]);
	$comments = htmlspecialchars($_POST["comments"]);
	$company_id = encryptorDecryptor('decrypt',$_POST["company_id"]);
	$path = date('Y').'/'.date('m').'/';
	if (!file_exists('../uploads/'.$path)) {
	    mkdir(('../uploads/'.$path), 0777, true);
	    $target = '../uploads/'.$path;
	} else {
	    $target = '../uploads/'.$path;
	}

	
	$company_logo_fetch = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tb_company WHERE company_id = '".$company_id."'"));
	$company_logo = '../logo/'.$company_logo_fetch['logo_path'].$company_logo_fetch['company_logo'];
	$image_array = array();
	$file_names = array();
	if(isset($_FILES['file'])) {
		foreach($_FILES['file']['tmp_name'] as $key => $tmp_name) {
			$file_name     = $_FILES['file']['name'][$key];
			$file_size     = $_FILES['file']['size'][$key];
			$file_tmp      = $_FILES['file']['tmp_name'][$key];
			$file_type     = $_FILES['file']['type'][$key];
			
			$parts         = pathinfo(basename($_FILES['file']['name'][$key]));
			$file_id       = date('d').time().mt_rand(9,99);
			$file_new_name = "FP-ID".$prj_id.'-'.$file_id.".".$parts['extension'];
			$img           = $target.$file_new_name;
			$file_names[]  = $img;

			move_uploaded_file($file_tmp,$img);

			if ($company_logo_fetch['company_logo'] != '') {
				$imageGrid = new imageGrid(2800, 2700, 280, 270);
				if ($parts['extension'] == 'jpg') {
					$img1 = imagecreatefromjpeg($img);
					$imageGrid->putImage($img1, 280, 210, 0, 0);
				} else if ($parts['extension'] == 'png') {
					$img1 = imagecreatefrompng($img);
					$imageGrid->putImage($img1, 280, 210, 0, 0);
				}
				$img2 = imagecreatefromjpeg($company_logo);
					$imageGrid->putImage($img2, 280, 60, 0, 210);

				$collageFlag = $imageGrid->display($img);
				imagedestroy($img1);
				imagedestroy($img2);
			}
			array_push($image_array, $file_new_name);
			$tb_final_img = mysqli_query($con, "INSERT INTO tb_final_img(final_image_name, prj_id, final_file_path, final_img_type) VALUES ('".$file_new_name."','".$prj_id."','".$path."','".$parts['extension']."')");
		}
		$image_series = join(',', $image_array);
		$tb_comment = mysqli_query($con, "INSERT INTO `tb_comment`(`project_id`, `comment_type`, `image_name`, `image_path`, `comments`, `user`) VALUES ('".$prj_id."', 'final', '".$image_series."', '".$path."', '".$comments."', '".$_SESSION['name']."')");

		// Mail Configuration
		$mail_query = mysqli_query($con, "SELECT * FROM `email_templates` WHERE tpltype = 'DELIVERCONFIRMATIONTOCLIENT' AND mailtype = 'User'");
		$project = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tb_record WHERE prj_id = '".$prj_id."'"));
		$email = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM `tb_user` WHERE id = '".$project['client_id']."'"));
		$rows           = mysqli_fetch_assoc($mail_query);

		$user_parum     = array(' __PROJECTID__',' __COMPANYNAME__',' __ADDRESS__');
		$user_parum_sub = array(' __PROJECTID__',' __ADDRESS__');
		$data           = base64_decode($rows['messagebody']);
		$data_sub       = $rows['subject'];
		$msg            = stripslashes(html_entity_decode($data,ENT_QUOTES));
		$msg_sub        = stripslashes(html_entity_decode($data_sub,ENT_QUOTES));
		$user_rep       = array($prj_id,$company_logo_fetch['company_name'],$project['project_name']);
		$user_rep_sub   = array($prj_id,$project['project_name']);
		$email_message  = str_replace($user_parum,$user_rep,$msg);
		$email_message_sub  = str_replace($user_parum_sub,$user_rep_sub,$msg_sub);
		$mail           = new PHPMailer();
		$mail->Subject  = $email_message_sub;
		$mail->From     = $rows['mailfrom'];
		$mail->FromName = $rows['mailfromname'];

		foreach ($file_names as $value) {
			$attachmentLink = '../uploads/'.$value;
			$mail->addAttachment($attachmentLink);
		}
		$email_id = $email['email'];
		$mail->addAddress($email_id);
		$mail->Body = $email_message;
		$mail->IsHTML(true);
		$mail->Send();

		echo json_encode(array("data"=>"success","message"=>"<p style='color:green;'>Output Blueprints Uploaded Succesfully</p>"));
	} else {
		echo json_encode(array("data"=>"error","message"=>"<p style='color:red;'><i class='icon-remove-sign'></i> Please select files!</p>"));
	}
?>